package group.spartan.questiongame.api;
import group.spartan.questiongame.api.quizframework.MultipleChoiceQuestion;
import group.spartan.questiongame.api.quizframework.Question;
import group.spartan.questiongame.api.quizframework.QuestionType;
import group.spartan.questiongame.api.quizframework.Quiz;

import java.io.*;
import java.util.*;

public class QuizManager
{
    private HashMap<String, Quiz> quizHashMap;

    public QuizManager()
    {
        this.quizHashMap = new HashMap<>();
    }

    public boolean loadFile(File file)
    {
        if(file.exists())
        {
            try
            {
                BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
                String line;

                //local

                //quiz variables
                String quizName = "";
                List<Question> questionList = new ArrayList<>();

                //question variables
                List<String> alternateAnswers = new ArrayList<>();
                String question, type, answer;
                question = type = answer = "";

                while( (line = bufferedReader.readLine()) != null )
                {
                    if(line.contains("<quizzes") || line.contains("<quiz>"))
                    {
                        continue;
                    }

                    if(line.contains("</quiz>"))
                    {
                        this.quizHashMap.put(quizName, new Quiz(questionList));
                        questionList = new ArrayList<>();
                        continue;
                    }

                    if(line.contains("<name>"))
                    {
                        quizName = trimTags(line, "<name>", "</name>");
                        continue;
                    }

                    if(line.contains("<quizquestion>"))
                    {
                        continue;
                    }

                    if(line.contains("<type>"))
                    {
                        type = trimTags(line, "<type>", "</type>");
                        continue;
                    }

                    if(line.contains("<question>"))
                    {
                        question = trimTags(line, "<question>", "</question>");
                        continue;
                    }

                    if(line.contains("answer"))
                    {
                        answer = trimTags(line, "<answer>", "</answer>");
                        continue;
                    }

                    QuestionType questionType = QuestionType.produceType(type);

                    switch (questionType)
                    {
                        case SINGLE:
                            questionList.add(new Question(question, answer));
                            break;
                        case MULTIPLE:
                            for (int i = 0; i < 3; i++)
                            {
                                alternateAnswers.add(trimTags(line, "<alternateanswer>", "</alternateanswer>"));
                                line = bufferedReader.readLine();
                            }

                            questionList.add(new MultipleChoiceQuestion(question, answer, alternateAnswers));
                            break;
                    }
                }
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }

        return false;
    }

    public String trimTags(String line, String tag, String closingTag)
    {
        if(line != null)
        {
            line = line.replaceAll("\\s+", " ");
            line = line.replaceAll(tag, "");
            line = line.replaceAll(closingTag, "");

            return line.substring(1);
        }

        return null;
    }

    public Optional<Quiz> getQuiz(String quizName)
    {
        if(this.quizHashMap.containsKey(quizName))
        {
            return Optional.of(this.quizHashMap.get(quizName));
        }

        return Optional.empty();
    }

    public Set<String> getQuizNames()
    {
        return this.quizHashMap.keySet();
    }

    public int getAmountOfQuizzes()
    {
        return this.quizHashMap.size();
    }
}
