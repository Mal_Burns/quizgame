package group.spartan.questiongame.api.quizframework;

import java.util.List;
import java.util.Optional;

public class Quiz
{
    private List<Question> questionList;
    private int count;

    public Quiz(List<Question> questionList)
    {
        this.questionList = questionList;
        this.count = 0;
    }

    public List<Question> getQuestionList()
    {
        return this.questionList;
    }

    public boolean hasNext()
    {
        return count <= questionList.size();
    }


    public Question getNext()
    {
        Question question = this.questionList.get(count);
        this.count++;

        return question;
    }

    public void resetCounter()
    {
        this.count = 0;
    }
}
