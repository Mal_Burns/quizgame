package group.spartan.questiongame.api.quizframework;

public enum QuestionType
{
    SINGLE, MULTIPLE;

    public static QuestionType produceType(String type)
    {
        switch (type.toLowerCase())
        {
            case "single":
                return SINGLE;
            case "multiple":
                return MULTIPLE;
        }

        return null;
    }
}
