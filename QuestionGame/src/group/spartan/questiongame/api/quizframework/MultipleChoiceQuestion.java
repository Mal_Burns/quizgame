package group.spartan.questiongame.api.quizframework;

import java.util.List;

public class MultipleChoiceQuestion extends Question
{
    private List<String> alternateAnswers;

    public MultipleChoiceQuestion(String question, String answer, List<String> alternateAnswers)
    {
        super(question, answer);
        this.alternateAnswers = alternateAnswers;
    }

    public List<String> getAlternateAnswers()
    {
        return this.alternateAnswers;
    }
}
