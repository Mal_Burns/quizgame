package group.spartan.questiongame.api.quizframework;

public class Question
{
    private String question, answer;

    public Question(String question, String answer)
    {
        this.question = question;
        this.answer = answer;
    }

    public boolean isAnswer(String userAnswer)
    {
        return this.answer.equalsIgnoreCase(userAnswer);
    }

    public String getQuestion()
    {
        return this.question;
    }
}
