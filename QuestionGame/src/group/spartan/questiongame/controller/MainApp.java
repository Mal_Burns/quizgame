package group.spartan.questiongame.controller;

import group.spartan.questiongame.api.QuizManager;
import group.spartan.questiongame.api.quizframework.Question;
import group.spartan.questiongame.api.quizframework.Quiz;

import java.io.File;
import java.util.List;

public class MainApp
{
    public static void main(String[] args)
    {
        QuizManager quizManager = new QuizManager();
        quizManager.loadFile(new File("C:\\Users\\Malachy\\Desktop\\College year 3\\Distributed\\QuestionGame\\src\\large.xml"));

        for(String string : quizManager.getQuizNames())
        {
            System.out.println("Quizname: " + string);
            List<Question> questionList = quizManager.getQuiz(string).get().getQuestionList();

            for(Question question : questionList)
            {
                System.out.println(question.getQuestion());
            }
        }
    }
}
